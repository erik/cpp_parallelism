# Each .cpp file is independent.
# Run "make thread", "make pi_threads", etc. to build one.

CPPFLAGS=-Wall -O3 -pthread -std=c++11 -ffast-math

%: %.cpp
	g++ $(CPPFLAGS) -o build/$@ $<
 
