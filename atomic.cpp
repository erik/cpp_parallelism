#include <array>
#include <thread>
#include <iostream>

int main() {
    constexpr auto n_threads = 10u;
    std::atomic<int> counter = {0};
    std::array<std::thread, n_threads> threads;

    const auto increment = [](std::atomic<int>& i) {
        // We don't care what order the the threads perform their increment operations.
        i.fetch_add(1, std::memory_order_relaxed);
    };

    for (auto i = 0u; i < n_threads; ++i) {
        threads[i] = std::thread(increment, std::ref(counter));
    }

    for (auto i = 0u; i < n_threads; ++i) {
        threads[i].join();
    }

    std::cout << "Final value: " << counter << std::endl;

    return 0;
}

