#include <thread>
#include <iostream>

void thread_method(int i) {
    std::cout << "hello thread " << i << std::endl;
}

struct ThreadObject {
    void operator()(int i) const {
        std::cout << "hello object " << i << std::endl;
    }
};

int main() {
    // Note that no care is taken to avoid race conditions around the global object std::cout.
    // See mutex.cpp for a solution.
    // Note that arguments are passed by value to the child threads.
    auto function_thread = std::thread(thread_method, 1);
    auto object_thread = std::thread(ThreadObject(), 2);
    auto lambda_thread = std::thread([](int i) {
        std::cout << "hello lambda " << i << std::endl;
    }, 3);

    // Without these, the main thread could terminate before the children complete.
    function_thread.join();
    object_thread.join();
    lambda_thread.join();

    return 0;
}

